import React from 'react';
import './App.css';
import { ModelViewer } from './modules/model-viewer';

function App() {
  return (
    <ModelViewer/>
  );
}

export default App;
