import React, { Component }  from 'react'
import * as THREE from "three";
import ReactDOM from 'react-dom';
import CubeView from 'react-cubeview';
import {SketchPicker} from 'react-color'
import '../../node_modules/react-cubeview/lib/css/react-cubeview.css';
import Container3d from 'react-container-3d';

let OrbitControls = require("react-cubeview/lib/OrbitControls")(THREE)
let container, controls, scene, camera, renderer, cube, group;

export class ModelViewer extends Component {
  constructor(props) {
    super(props)
    let presets = {}
    presets.color = 'red'
    presets.cubesCount = 1
    presets.cubesPosition = { x: 0.0, y: 1.0, z: 0.0 }
    this.state = presets
  }

  handleChange = (event) => {
    this.setState({color: event.hex})
    this.group.children.forEach(mesh => {
      if (mesh.name === 'cubes') {
        mesh.material = new THREE.MeshStandardMaterial ( { color: event.hex } );
      }
    })
    // this.extractObject(this.scene, this.camera, this.renderer, event.hex)
  }

  handleClick = (event) => {
    event.preventDefault()
    this.setState({cubesPosition: { x: this.state.cubesCount, y: 1.0, z: 0.0 }, cubesCount: this.state.cubesCount + 1})
    this.addCube(this.scene, this.group, this.state.color)
  }

  removeBox = (event) => {
    event.preventDefault()
    if (this.group.children.length > 2) {
      this.group.children.pop(1)
      this.setState({cubesCount: this.state.cubesCount - 1})
    }
  }

  Setup = (scene, camera, renderer) =>{
    this.scene = scene
    this.camera = camera
    this.renderer = renderer
    this.extractObject(scene, camera, renderer, this.state.color)
  }

  extractObject = (scene, camera, renderer, color) => {
    const group = new THREE.Group()
    this.group = group
    // Creating ground for scene
    let ground_geometry = new THREE.BoxGeometry( 0, 0, 0  );
    let ground_material = new THREE.MeshPhongMaterial( { color: 0x99ff99 } );
    const ground = new THREE.Mesh( ground_geometry, ground_material );
    ground.receiveShadow = false;
    group.add(ground)
    ground.receiveShadow = true;

    this.addCube(scene, group, color)

    renderer.setSize( window.innerWidth, window.innerHeight );
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.PCFShadowMap;
    // renderer.render(camera, container)
  }

  addCube = (scene, group, color) => {
    // Creating Cube for ground
    let cube_geometry = new THREE.BoxGeometry( 2, 2, 2  );
    let cube_material = new THREE.MeshStandardMaterial ( { color: color } );
    const cube = new THREE.Mesh( cube_geometry, cube_material );
    cube.castShadow = true;
    cube.name = 'cubes'
    cube.position.set( this.state.cubesPosition.x, this.state.cubesPosition.y, this.state.cubesPosition.z );
    group.add(cube)
    scene.add(group)    
  }

  getDomContainer = () =>{
    return ReactDOM.findDOMNode(container);
  }

  getDomCube() {
      return ReactDOM.findDOMNode(cube);
  }

  Update = (scene, camera, renderer) => {
    // console.log(controls.enableRotate)
    if (scene && camera && renderer) {
        renderer.render(scene, camera)
        this.setState({ scene: scene, camera: camera, renderer: renderer, controls: controls })
    }
  }

  render() {
    return (
      <div className="content-wrapper container" id="container3dViewer">
          <SketchPicker onChange={ this.handleChange }/>
          <button style={{background: this.state.color, color: 'white'}} onClick={this.handleClick}>Click to add box</button>
          <button style={{background: this.state.color, color: 'white'}} onClick={this.removeBox}>Click to remove box</button>
          <Container3d
              className="canvas-3d"
              percentageWidth={'100%'}
              fitScreen
              ref={c => (container = c)}
              marginBottom={30}
              addLight={true}
              addControls={true}
              addGrid={true}
              antialias={true}
              onUpdateAngles={this.updateAngles2}
              onHoverStart={this.onHoverStart}
              onHoverEnd={this.onHoverEnd}
              setup={this.Setup}
              update={this.Update}
          />

          <div className="cube-view">
              <CubeView
                  aspect={1}
                  hoverColor={0x0088ff}
                  cubeSize={2}
                  ref={c => (cube = c)}
                  zoom={6}
                  antialias={false}
                  key={'cv'}
                  width={150}
                  height={150}
                  onUpdateAngles={this.updateAngles}
              />
          </div>
      </div>
    )
  }
}